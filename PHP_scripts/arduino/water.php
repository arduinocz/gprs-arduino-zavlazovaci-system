<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);


include_once ("../php/variables.php");
include_once ("../arduino/functions.php");

$conn = mysqli_connect($myHost, $myUser, $myPassword,$myDb);
//$value1 = $_GET["data"];

//$pole=explode("*",$value1);

$i = 0;



//var_dump ($pole);


if(! $conn )
{
  die('Could not connect: ' . mysqli_error());
}


$sql = "SELECT id, status, packetDestination, packetDataType, packetTime, packetData, completedTime FROM waitingCommands WHERE ((status=0 OR status=1) AND (packetDataType=4)) ORDER BY packetDestination, timestamp ASC";

	  $retval = mysqli_query( $conn, $sql);
	  if(! $retval )
	  {
	    die('Could not retrive data: ' . mysqli_error($conn));
	  }

          $i=0;
	  while($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
		//if ($row[$i]) {
		$commandsDestinationID [$i] = $row["packetData"]%10;
		$commandsToDisplay[$i] = "Entered time: ". (int) ($row["packetTime"]/60) .":". $row["packetTime"]%60 ." - Watering circuit: ". $row["packetData"]%10 ." - Watering duration: ". ((int) ($row["packetData"]/10)) * 3 . " minutes.";
		$i++;

	  }


?>

<div class="formular">
  <h2><span>Circuit 1</span></h2>
  
  <h3><span>Currently waiting commands</span></h3>
  <?php
    if ($i>0) {
      $i=0;
      foreach ($commandsDestinationID as $comID) {
        if ($comID == 1) {
          echo $commandsToDisplay[$i]. "<BR>";
        }
      $i++;
      } 
    } else {
      echo "No commands.";
    }
  ?>
</div>
<BR>

<div class="formular">
  <h2><span>Circuit 2</span></h2>
  
  <h3><span>Currently waiting commands</span></h3>
  <?php
    if ($i>0) {
      $i=0;
      foreach ($commandsDestinationID as $comID) {
        if ($comID == 2) {
          echo $commandsToDisplay[$i]. "<BR>";
        }
      $i++;
      } 
    } else {
      echo "No commands.";
    }
  ?>
</div>
<BR>



<div class="formular">
  <h2><span>Circuit 3</span></h2>
  
  <h3><span>Currently waiting commands</span></h3>
  <?php
    if ($i>0) {
      $i=0;
      foreach ($commandsDestinationID as $comID) {
        if ($comID == 3) {
          echo $commandsToDisplay[$i]. "<BR>";
        }
      $i++;
      } 
    } else {
      echo "No commands.";
    }
  ?>
</div>
<BR>


<div class="formular">
  <h2><span>Schedule watering for circuit</span></h2>
  <form action="getdata.php?data=3*WEB*4" method="post"> <?php //3 - client, WEB - command from web, 4 - watering, 1 - which circuit ?>
    Watering circuit (select 1 - 3): <input type="text" name="circuit" size="1" value=""><BR> 
    Watering for 1-12 (1 = 3 minutes, 12 = 36 minutes): <input type="text" name="duration" size="2" value="1"><BR><BR>
    <input type="submit" value="Apply">

  <form>
</div>
