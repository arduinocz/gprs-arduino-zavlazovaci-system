<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);


include_once ("../php/variables.php");
include_once ("../arduino/functions.php");

$conn = mysqli_connect($myHost, $myUser, $myPassword,$myDb);
//$value1 = $_GET["data"];

//$pole=explode("*",$value1);

if (isset ($_GET["selectedDate"])) {
  $dateToDisplay = $_GET["selectedDate"];
} else {
  $dateToDisplay = date('Y-m-d');
}

$i = 0;

// ONLY FOR TESTING - zakomentovat
$dateToDisplay = '2015-08-22';


echo "Home Page";
?>

<form action="index.php?page=home">
Select date to be displayed<br>
<input type="date" name="selectedDate" value="<?php echo $dateToDisplay ?>">
<br>
<input type="submit" value="Refresh">
</form>


<?php

//var_dump ($pole);


if(! $conn )
{
  die('Could not connect: ' . mysqli_error());
}


//$sql = "SELECT AVG(hodnota) AS averageValue, timestamp FROM arduinoData GROUP BY DATE(timestamp), HOUR(timestamp)";


// Temperature sensor - Cli03
$sql = "SELECT HOUR(timestamp) AS `hour`, AVG(hodnota) AS averageValue FROM arduinoData WHERE (DATE(timestamp) = '$dateToDisplay' - INTERVAL 1 DAY) AND (client='Cli03') AND (velicina='T') GROUP BY hour ORDER BY hour ASC";

	  $retval = mysqli_query( $conn, $sql);
	  if(! $retval )
	  {
	    die('Could not retrive data: ' . mysqli_error($conn));
	  }

          $i=0;
	  while($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
		$dataToGraph1 [] = $row['hour'];
		$dataToGraph2 [] = round($row['averageValue']);
		//var_dump ($row);
	  }

//var_dump ($dataToGraph1);



$sql = "SELECT HOUR(timestamp) AS `hour`, AVG(hodnota) AS averageValue FROM arduinoData WHERE (DATE(timestamp) = '$dateToDisplay' - INTERVAL 1 DAY) AND (client='Cli03') AND (velicina='H') GROUP BY hour ORDER BY hour ASC";

	  $retval = mysqli_query( $conn, $sql);
	  if(! $retval )
	  {
	    die('Could not retrive data: ' . mysqli_error($conn));
	  }

          $i=0;
	  while($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
		$dataToGraph3 [] = $row['hour'];
		$dataToGraph4 [] = round($row['averageValue']);
		//var_dump ($row);
	  }

//var_dump ($dataToGraph1);


?>


<div id="container-A" style="width:100%; height:400px;"></div>
<div id="container-B" style="width:100%; height:400px;"></div>
<div id="container-C" style="width:100%; height:400px;"></div>
<div id="container-D" style="width:100%; height:400px;"></div>

<script>
$(function () {
    $('#container-A').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Temperature (hourly)'
        },
        subtitle: {
            text: '<?php echo $dateToDisplay ?>'
        },
        xAxis: {
            categories: [<?php echo join($dataToGraph1, ',') ?>]
        },
        yAxis: {
            title: {
                text: 'Temperature (�C)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Glass house',
            data: [<?php echo join($dataToGraph2, ',') ?>]
        }, {
            name: 'Outside',
            data: [<?php echo join($dataToGraph2, ',') ?>]
        }]
    });


    $('#container-B').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Humidity (hourly)'
        },
        subtitle: {
            text: '<?php echo $dateToDisplay ?>'
        },
        xAxis: {
            categories: [<?php echo join($dataToGraph3, ',') ?>]
        },
        yAxis: {
            title: {
                text: 'Humidity'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Glass house',
            data: [<?php echo join($dataToGraph4, ',') ?>]
        }, {
            name: 'Outside',
            data: [<?php echo join($dataToGraph4, ',') ?>]
        }]
    });


    $('#container-C').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Soil moisture (hourly)'
        },
        subtitle: {
            text: '<?php echo $dateToDisplay ?>'
        },
        xAxis: {
            categories: [<?php echo join($dataToGraph1, ',') ?>]
        },
        yAxis: {
            title: {
                text: 'Moisture'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Glass house',
            data: [<?php echo join($dataToGraph2, ',') ?>]
        }, {
            name: 'Outside',
            data: [<?php echo join($dataToGraph2, ',') ?>]
        }]
    });



   $('#container-D').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Water in barel (daily)'
        },
        subtitle: {
            text: '<?php echo $dateToDisplay ?>'
        },
        xAxis: {
            categories: [<?php echo join($dataToGraph1, ',') ?>]
        },
        yAxis: {
            title: {
                text: 'Water (%)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Glass house',
            data: [<?php echo join($dataToGraph2, ',') ?>]
        }, {
            name: 'Outside',
            data: [<?php echo join($dataToGraph2, ',') ?>]
        }]
    });


});
</script>



