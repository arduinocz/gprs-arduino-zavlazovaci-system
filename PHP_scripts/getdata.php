<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);


include_once ("../php/variables.php");
include_once ("../arduino/functions.php");

$conn = mysqli_connect($myHost, $myUser, $myPassword,$myDb);
$value1 = $_GET["data"];

$pole=explode("*",$value1);


//var_dump ($pole);


if(! $conn )
{
  die('Could not connect: ' . mysqli_error());
}

//echo "post data:";
//echo $_POST["circuit"];



for ($i = 0; $i < count($pole); $i = $i + 4) {

switch ($pole[$i+1]) {
	/*
	case "X":
	  $sql = "SELECT id, packetDestination, packetTime, packetDataType, packetData FROM waitingCommands ORDER BY packetTime ASC";

	  $retval = mysqli_query( $conn, $sql);
	  if(! $retval )
	  {
	    die('Could not retrive data: ' . mysqli_error($conn));
	  }

	  while($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
	    //var_dump ($row);
	    //for ($i = 0; $i < count($row); $i = $i + 1) {
	    //echo "WaitComm*" . $row["client"] . "*" .$row["packetData"] . "*" . $row["id"] . "!";
	    echo packetDestination=$row["packetDestination"];
	    echo packetDataType=$row["packetDataType"];
	    echo packetTime=$row["packetTime"];
	    echo packetData=$row["packetData"];
	  } else {
	    echo "No commands to be executed.";
	  }

	  echo "END.";
	  break;
	*/
	
	/*
	case "F":
		$sql = "SELECT id, timestamp, client, command FROM waitingCommands WHERE id='{$pole[2]}' ORDER BY timestamp DESC LIMIT 1";
		$retval = mysqli_query( $conn, $sql);
		if(! $retval )
		{
	 	 die('Could not enter data: ' . mysqli_error($conn));
		}

		$row = mysqli_fetch_array($retval, MYSQLI_ASSOC);

		$sql = "INSERT INTO arduinoData (client, velicina, hodnota) VALUES ( '{$row['client']}', 'F', '{$row['command']}')";
		$retval = mysqli_query( $conn, $sql);
		if(! $retval )
		{
		  die('Could not enter data: ' . mysqli_error($conn));
		}

		$sql = "DELETE FROM waitingCommands WHERE id = '{$pole[2]}'";
		$retval = mysqli_query( $conn, $sql);
		if(! $retval )
		{
		  die('Could not delete data (command) from database: ' . mysqli_error($conn));
		}

	  break;
 	*/

	case "WEB": // Web Command - command entered from web pages
	  echo "Command from web received.<BR>";
	  $timeOfCommand = date('H') * 60 + date('i');
	  $timestamp = date('Y-m-d H:i:s');
	  $sql = "INSERT INTO waitingCommands (timestamp, status, packetDestination, packetDataType, packetTime, packetData) VALUES ('$timestamp', 0, '{$pole[0]}', '{$pole[2]}', '{$timeOfCommand}', '{$_POST['duration']}{$_POST['circuit']}')";

	  $retval = mysqli_query( $conn, $sql);
	  if(! $retval )
	  {
	    die('Could not enter data (command): ' . mysqli_error($conn));
	  }
	  echo "Entered data successfull. <a href='index.php?page=wate'>Continue</a>";
	  goto endOfPage;
	break;


	case "SET": // Settings from web - command entered from web pages
	  echo "Settings from web received.<BR>";

	  //PART 1 -------------------
	  //$sql = "INSERT INTO waitingCommands (status, packetDestination, packetDataType, packetTime, packetData) VALUES ( 0, '{$pole[0]}', '{$pole[2]}', '{$timeOfCommand}', '{$_POST['duration']}{$_POST['circuit']}')";
	  $sql = "SELECT clientID, settingsID, value FROM settings ORDER BY clientID ASC";
	  $retval = mysqli_query( $conn, $sql);
	  if(! $retval )
	  {
	    die('Could not get data (command): ' . mysqli_error($conn));
	  }
	  echo "Data red successfully.<BR>";
	  
	  while($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
	   if (isset($_POST['settings1'])) {
	    if ($row["settingsID"] == 1 &&  $row["value"] != $_POST['settings1']) { // GPRS settings changed

	      $timeOfCommand = date('H') * 60 + date('i');
	      $timestamp = date('Y-m-d H:i:s');
	      $sql = "INSERT INTO waitingCommands (timestamp, status, packetDestination, packetDataType, packetTime, packetData) VALUES ('$timestamp', 0, '{$pole[0]}', 6, '{$timeOfCommand}', '{$_POST['settings1']}')";

	      $retval1 = mysqli_query( $conn, $sql);
	      if(! $retval1 )
	      {
	        die('Could not enter data (command): ' . mysqli_error($conn));
	      }
	      echo "GPRS settings successfully saved in waiting commands.<BR>";
	    
	    }
	   }

	   if (isset($_POST['settings2'])) {
	    if ($row["settingsID"] == 2 &&  $row["value"] != ($_POST['settings2']+7)) { // NRF active duration settings changed

	      $timeOfCommand = date('H') * 60 + date('i');
	      $realValue = $_POST["settings2"] + 7;
	      $timestamp = date('Y-m-d H:i:s');
	      $sql = "INSERT INTO waitingCommands (timestamp, status, packetDestination, packetDataType, packetTime, packetData) VALUES ('$timestamp', 0, '{$pole[0]}', 5, '{$timeOfCommand}', '$realValue')";

	      $retval1 = mysqli_query( $conn, $sql);
	      if(! $retval1 )
	      {
	        die('Could not enter data (command): ' . mysqli_error($conn));
	      }
	      echo "NRF active duration settings successfully saved in waiting commands.<BR>";
	    
	    }
	   }

	   if (isset($_POST['settings3'])) {
	    if ($row["settingsID"] == 3 &&  $row["value"] != $_POST['settings3']+1) { // NRF sleep duration settings changed

	      $timeOfCommand = date('H') * 60 + date('i');
	      $realValue = $_POST["settings3"] + 1;
	      $timestamp = date('Y-m-d H:i:s');
	      $sql = "INSERT INTO waitingCommands (timestamp, status, packetDestination, packetDataType, packetTime, packetData) VALUES ('$timestamp', 0, '{$pole[0]}', 5, '{$timeOfCommand}', '$realValue')";

	      $retval1 = mysqli_query( $conn, $sql);
	      if(! $retval1 )
	      {
	        die('Could not enter data (command): ' . mysqli_error($conn));
	      }
	      echo "NRF sleep duration settings successfully saved in waiting commands.<BR>";
	    
	    }
	   }

	  }

	  echo "WEB settings proceed successfully. <a href='index.php?page=sett'>Continue</a>";
	  goto endOfPage;
	break;


	case "3": 			// Packet with command
	  switch ($pole[$i+3]) {
	    case 1:	 		// Reqest actual date and time
	      displayDateTime();
	    break;
	    
	    case 2:	 		// Reqest actual date and time
	      displayDateTime();
	    break;
	  }
	  
	  switch ($pole[$i]) {
	    case 1:
	      $realClient = "Cli01";
	    break;
	    case 2:
	      $realClient = "Cli02";
	    echo "client02";
	    break;
	    case 3:
	      $realClient = "Cli03";
	    break;
	    case 4:
	      $realClient = "Cli04";
	    break;
	    case 5:
	      $realClient = "Cli05";
	    break;
	  }

	  echo $realClient."\r\n";


	  // Now check also for commands waiting in the database and attache them to displayed page


	  //displayWaitingCommands(0);  // Check for waiting commands and display them - 0 means all waiting commands for all clients

	  echo "ENDPAGE.\r\n";
	  break;




	  break;

	default:

	  switch ($pole[$i]) {
	    case 1:
	      $realClient = "Cli01";
	    break;
	    case 2:
	      $realClient = "Cli02";
	    echo "client02";
	    break;
	    case 3:
	      $realClient = "Cli03";
	    break;
	    case 4:
	      $realClient = "Cli04";
	    break;
	    case 5:
	      $realClient = "Cli05";
	    break;
	    default:
	      $realClient = "Cli??";
	      echo "Unknow real client. Received number in field pole: " . $pole[$i];
	  }


	  switch ($pole[$i + 1]) {
	    case 15:
	      $realVelicina = "T";
	    break;
	    case 14:
	      $realVelicina = "H";
	    break;
	    case 13:
	      $realVelicina = "L";
	    break;
	    case 12:
	      $realVelicina = "B";
	    break;
	    
	    case 5:
	      $realVelicina = "S";
	    break;

	    case 4:
	      $realVelicina = "V";
	    break;

	    default:
	      $realVelicina = "N";
	      
	  }

	  //$sql = "INSERT INTO arduinoData (client, velicina, timestamp, hodnota) VALUES ( '{$pole[0]}', '{$pole[1]}', '{$pole[2]}', '{$pole[3]}')";

	  $sql = "INSERT INTO arduinoData (client, velicina, timestamp, hodnota) VALUES ( '{$realClient}', '{$realVelicina}', '{$pole[$i + 2]}', '{$pole[$i + 3]}')";

	  $retval = mysqli_query( $conn, $sql);
	  if(! $retval )
	  {
	    die('Could not enter data: ' . mysqli_error($conn));
	  }

	  //----------------------- in case return packet (wattering, settings...) received then update related record in waitingCommands table
	  
	  if ($pole[$i + 1] == 4 || $pole[$i + 1] == 5 || $pole[$i + 1] == 6) {
	    $sql = "UPDATE waitingCommands SET completedTime = '{$pole[$i + 2]}', status = 2 WHERE (status = 1) AND (packetDataType = '{$pole[$i+1]}') AND (packetData = '{$pole[$i+3]}')";

	    $retval = mysqli_query( $conn, $sql);
	    if(! $retval )
	    {
	      die('Could not enter data: ' . mysqli_error($conn));
	    } 
	  }


	  //----------------------- in case return packet is settings packet received then update settings table
	  //----NRF active duration (8 - 13)

	  if (($pole[$i + 1] == 5) && ($pole[$i+3] > 7) && ($pole[$i+3] < 14)) {
	    $sql = "UPDATE settings SET value = '{$pole[$i+3]}' WHERE ((clientID = '{$pole[$i]}') AND (settingsID = 2))";

	    $retval = mysqli_query( $conn, $sql);
	    if(! $retval )
	    {
	      die('Could not enter data: ' . mysqli_error($conn));
	    } 
	  }
	  
	  //----NRF sleep duration (2 - 7)
	  if ($pole[$i + 1] == 5 && ($pole[$i+3] > 1) && ($pole[$i+3] < 8)) {
	    $sql = "UPDATE settings SET value = '{$pole[$i+3]}' WHERE ((clientID = '{$pole[$i]}') AND (settingsID = 3))";

	    $retval = mysqli_query( $conn, $sql);
	    if(! $retval )
	    {
	      die('Could not enter data: ' . mysqli_error($conn));
	    } 
	  }

	  //----GPRS connection interval (1 - 250)
	  if ($pole[$i + 1] == 6) {
	    $sql = "UPDATE settings SET value = '{$pole[$i+3]}' WHERE ((clientID = '{$pole[$i]}') AND (settingsID = 1))";

	    $retval = mysqli_query( $conn, $sql);
	    if(! $retval )
	    {
	      die('Could not enter data: ' . mysqli_error($conn));
	    } 
	  }


	break;


}

} 





echo "Entered data successfull. Number of records: ". ($i / 4);
	  

// Now also check for waiting commands and send them. 0 means all waiting commands for all clients


$sql = "SELECT id, packetDestination, packetTime, packetDataType, packetData FROM waitingCommands WHERE status=0 ORDER BY packetTime ASC";
$retval = mysqli_query( $conn, $sql);
if(! $retval )
{
  die('Could not retrive data: ' . mysqli_error($conn));
}
	  
echo "<BR>\r\n";;
echo "Start sending packets (commands) to arduino.<BR>\r\n";
while($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
	//var_dump ($row);
	//for ($i = 0; $i < count($row); $i = $i + 1) {
	//echo "WaitComm*" . $row["client"] . "*" .$row["packetData"] . "*" . $row["id"] . "!";
	echo "packetDestination=".$row["packetDestination"]."\r\n";
	echo "packetDataType=".$row["packetDataType"]."\r\n";
	echo "packetTime=".$row["packetTime"]."\r\n";
	echo "packetData=".$row["packetData"]."\r\n";
	echo "ENDPACKETDATA.\r\n";
}
	
displayDateTime();


// Now also update status of send commands from waitingCommands table. 1 means that command has been send to requester (garden main point)

$sql = "UPDATE waitingCommands SET status = 1 WHERE status = 0";
$retval = mysqli_query( $conn, $sql);
if(! $retval )
{
  die('Could not retrive data: ' . mysqli_error($conn));
} else {
  echo "<BR>\r\n";;
  echo "Status of received commands updated in MySQL table.<BR>\r\n";
}

echo "ENDPAGE.\r\n";


endOfPage:

mysqli_close($conn);



?>